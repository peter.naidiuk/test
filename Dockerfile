FROM python
COPY . /app
RUN pip install -r /app/requirements.txt
EXPOSE 5000
ENTRYPOINT [ "python" ]

CMD [ "app/app.py" ]
